import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import {Utils} from '../../../../utils/Utils';

import './productList.html';
import {Products} from '../../../../api/products';

class ProductList {
    constructor($scope, $reactive, CompaniesService, ProductsService) {
        'ngInject';

        $reactive(this).attach($scope);

        this.ProductsService = ProductsService;

        this.helpers({
            products() {
                return Products.find({});
            },
            companies: () => CompaniesService.getViewModelsForCurrentUser()
        });

        //for refreshing view for now
        $(window).trigger('resize');
    }

    delete(id, name) {
        swal({
            title: "Jesteś pewien?",
            text: "Próbujesz usunąć " + name + ".",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Tak, usuń go!",
            cancelButtonText: "Anuluj",
            closeOnConfirm: false,
            html: false
        }, () => {
            this.ProductsService.remove(id, name);
        })
    }
}

const name = 'productList';

// create a module
export default angular.module(name, [
        angularMeteor,
        uiRouter
    ])
    .component(name, {
        templateUrl: Utils.getTemplatePath(name),
        controllerAs: 'vm',
        controller: ProductList
    })
    .config(config);

function config($stateProvider) {
    'ngInject';
    $stateProvider
        .state('admin.products', {
            url: '/Produkty',
            template: '<product-list></product-list>',
            resolve: {
                currentUser: ($q, $rootScope, $state, $timeout) => {
                    // 'ngInject';
                    // return $q.reject();
                    //  console.log($rootScope.currentUser, this);
                    // if ((!Meteor.userId() || !Roles.userIsInRole(Meteor.userId(), ['admin']))) {
                    //     $timeout(function() {
                    //         $state.go('admin.categories')
                    //     },0);
                    //     return $q.reject();
                    // }
                    // else {
                    //     return $q.resolve();
                    // }
                }
            }
        });
}
