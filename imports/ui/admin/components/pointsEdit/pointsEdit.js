import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import {Utils} from '../../../../utils/Utils';
import 'angular-simple-logger';
import 'angular-google-maps';

import './pointsEdit.html';

import {Points} from '../../../../api/points';

class PointsEdit {
    constructor($stateParams, PointsService, uiGmapIsReady) {
        'ngInject';

        this.PointsService = PointsService;
        this.point = PointsService.getPointById($stateParams.id);

        this.map = {
            center: {
                latitude: 50.8001241,
                longitude: 19.0623395
            },
            events: {}
        };

        uiGmapIsReady.promise()
            .then(() => {
                $('.geocomplete').geocomplete({
                        map: '.angular-google-map-container',
                        location: this.point.location.address
                    })
                    .bind("geocode:result", (event, result) => {
                        this.formattedAddress = result.formatted_address;
                        this.latitude = result.geometry.location.lat();
                        this.longitude = result.geometry.location.lng();
                    });
            });
    }

   updatePoint(vmPoint) {
        const point = {
            id: vmPoint._id,
            name: vmPoint.name,
            location: {
                type: 'point',
                address: this.formattedAddress,
                coordinates: [this.latitude, this.longitude]
            }
        };
        this.PointsService.update(point);
    }
}

const name = 'pointsEdit';

export default angular.module(name, [
        angularMeteor,
        uiRouter,
        'uiGmapgoogle-maps',
        'nemLogging'
    ])
    .component(name, {
        templateUrl: Utils.getTemplatePath(name),
        controllerAs: 'vm',
        controller: PointsEdit
    })
    
.config(['$stateProvider', 'uiGmapGoogleMapApiProvider', ($stateProvider, GoogleMapApi) => {
    GoogleMapApi.configure({
        key: 'AIzaSyAcE1bNUcOHQXeR9di2C4mkqQWnmCqPihA',
        libraries: 'places'
    });

    $stateProvider
        .state('admin.pointsEditor', {
            url: '/Punkty/Edycja/:id',
            template: '<points-edit></points-edit>'
        });
}])
