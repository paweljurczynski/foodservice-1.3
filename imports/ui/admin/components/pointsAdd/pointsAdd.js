import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import {Utils} from '../../../../utils/Utils';
import 'angular-simple-logger';
import 'angular-google-maps';
import './pointsAdd.html';

class PointsAdd {
    constructor(PointsService, uiGmapIsReady) {
        'ngInject';
        
        this.PointsService = PointsService;

        this.map = {
            center: {
                latitude: 50.8001241,
                longitude: 19.0623395
            },
            events: {}
        };
        
        uiGmapIsReady.promise()
            .then(() => {
                $('.geocomplete').geocomplete({
                        map: '.angular-google-map-container'
                    })
                    .bind("geocode:result", (event, result) => {
                        this.formattedAddress = result.formatted_address;
                        this.latitude = result.geometry.location.lat();
                        this.longitude = result.geometry.location.lng();
                    });
            });
    }

    addPoint(vmPoint) {
        const point = {
            name: vmPoint.name,
            location: {
                type: 'point',
                address: this.formattedAddress,
                coordinates: [this.latitude, this.longitude]
            }
        };
        this.PointsService.add(point);
    }
}

const name = 'pointsAdd';

// create a module
export default angular.module(name, [
        angularMeteor,
        uiRouter,
        'uiGmapgoogle-maps',
        'nemLogging'
    ])
    .component(name, {
        templateUrl: Utils.getTemplatePath(name),
        controllerAs: 'vm',
        controller: PointsAdd
    })
    .config(['$stateProvider', 'uiGmapGoogleMapApiProvider', ($stateProvider, GoogleMapApi) => {
        GoogleMapApi.configure({
            key: 'AIzaSyAcE1bNUcOHQXeR9di2C4mkqQWnmCqPihA',
            libraries: 'places'
        });

        $stateProvider
            .state('admin.pointsAdd', {
                url: '/Punkty/Dodaj',
                template: '<points-add></points-add>'
            });
    }])