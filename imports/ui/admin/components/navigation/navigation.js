/**
 * Created by pawel on 28.05.16.
 */
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import {Utils} from '../../../../utils/Utils';

import './navigation.html';

class Navigation {
    constructor($scope, $reactive, OrdersServiceAdmin) {
        'ngInject';

        $reactive(this).attach($scope);
        this.OrdersServiceAdmin = OrdersServiceAdmin;
        this.helpers({
            newOrdersAmount: () => OrdersServiceAdmin.getNewOrdersCount()
        });
    }
    
}



const name = 'navigation';

// create a module
export default angular.module(name, [
        angularMeteor,
        uiRouter
    ])
    .component(name, {
        templateUrl: Utils.getTemplatePath(name),
        controllerAs: 'vm',
        controller: Navigation
    });
