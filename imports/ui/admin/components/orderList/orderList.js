/**
 * Created by pawel on 28.05.16.
 */
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import {Utils} from '../../../../utils/Utils';

import './orderList.html';

class OrderList {
    constructor($scope, $reactive, OrdersServiceAdmin) {
        'ngInject';
        $reactive(this).attach($scope);
        this.OrdersServiceAdmin = OrdersServiceAdmin;
        this.helpers({
            orders: () => OrdersServiceAdmin.getOrdersByStatus('new')
        });
    }

    delete(id, number) {
        swal({
            title: "Jesteś pewien?",
            text: "Próbujesz usunąć zamówienie nr " + number,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Tak, usuń je!",
            cancelButtonText: "Anuluj",
            closeOnConfirm: false,
            html: false
        }, () => {
            this.OrdersServiceAdmin.remove(id, number);
        })
    }

    done(id, number){
        swal({
            title: "Czy zamówienie jest gotowe?",
            type: "info",
            showCancelButton: true,
            confirmButtonText: "Tak",
            cancelButtonText: "Nie",
            closeOnConfirm: false
        },()=>{
            this.OrdersServiceAdmin.done(id, number);
        })
    }
    
}

const name = 'orderList';

// create a module
export default angular.module(name, [
        angularMeteor,
        uiRouter
    ])
    .component(name, {
        templateUrl: Utils.getTemplatePath(name),
        controllerAs: 'vm',
        controller: OrderList
    })
    .config(config);

function config($stateProvider) {
    'ngInject';
    $stateProvider
        .state('admin.orders', {
            url: '/Zamowienia',
            template: '<order-list></order-list>'
        });
}
