/**
 * Created by pawel on 28.05.16.
 */
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import buzz from 'buzz';
import {Utils} from '../../../../utils/Utils';

import {notificationsAmount} from '../../../../api/messages';
import './message.html';

class Message {
    constructor($state, $scope, $reactive, MessageService) {
        'ngInject';

        $reactive(this).attach($scope);

        this.$state = $state;
        this.MessageService = MessageService;
        this.helpers({
            message: () => MessageService.getLastMessage(),
            shouldShow: () => MessageService.shouldDisplayMessage(),
        });

    }

    isOrders() {
        if (this.$state.includes('admin.orders')) {
            this.shouldShow = false;
            return true;
        }
        return false
    }

    hideMessages() {
        this.shouldShow = false;
    }
}

const name = 'message';

// create a module
export default angular.module(name, [
        angularMeteor,
        uiRouter
    ])
    .component(name, {
        templateUrl: Utils.getTemplatePath(name),
        controllerAs: 'vm',
        controller: Message
    });
