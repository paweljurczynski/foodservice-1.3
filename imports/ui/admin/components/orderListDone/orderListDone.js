/**
 * Created by pawel on 28.05.16.
 */
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import {Utils} from '../../../../utils/Utils';

import './orderListDone.html';

class OrderListDone {
    constructor($scope, $reactive, OrdersServiceAdmin) {
        'ngInject';

        $reactive(this).attach($scope);
        this.OrdersServiceAdmin = OrdersServiceAdmin;
        this.helpers({
            orders: () => OrdersServiceAdmin.getOrdersByStatus('done')
        });
    }
    deleteAll(){
        swal({
            title: "Czy na pewno chcesz usunąć wszystkie zamówienia z tej strony?",
            text: "Usuniesz tylko gotowe zamówienia. Oczekujące zamówienia pozostaną w zakładce Zamówienia",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            showCancelButton: true,
            confirmButtonText: "Tak",
            cancelButtonText: "Nie",
            closeOnConfirm: false
        },()=>{
            this.OrdersServiceAdmin.removeAll();
        })
    }
    

}

const name = 'orderListDone';

// create a module
export default angular.module(name, [
        angularMeteor,
        uiRouter
    ])
    .component(name, {
        templateUrl: Utils.getTemplatePath(name),
        controllerAs: 'vm',
        controller: OrderListDone
    })
    .config(config);

function config($stateProvider) {
    'ngInject';
    $stateProvider
        .state('admin.ordersDone', {
            url: '/ZamowieniaGotowe',
            template: '<order-list-done></order-list-done>'
        });
}
