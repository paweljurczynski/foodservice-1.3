import angular from 'angular';
import {Products} from '../../../api/products';

Meteor.subscribe('products');

class ProductsService {
    constructor($state, Notification) {
        'ngInject';
        this.$state = $state;
        this.Notification = Notification;
    }

    add(product) {
        Meteor.call('products.add', product, err => {
            if (err) {
                this.Notification.error(err.message);
            } else {
                this.Notification.success(`Produkt ${product.name} dodany!`);
                this.$state.go('admin.products');
            }
        });
    }

    update(product, id) {
        Meteor.call('products.update', product, id,  err => {
                if (err) {
                    this.Notification.error(err.message);
                } else {
                    this.Notification.success(`Produkt ${product.name}  zaktualizowany!`);
                    this.$state.go('admin.products');
                }
            }
        );
    }
    
    remove(id, name) {
        Meteor.call('products.remove', id,  err => {
                if (err) {
                    this.Notification.error(err.message);
                } else {
                    swal("Usunięto!", "Produkt " + name + " został usunięty.", "success");
                    this.$state.go('admin.products');
                }
            }
        );
    }

    getProductById(id) {
        return Products.findOne(id);
    }

    getUniqIngredients() {
        return _.uniq(_.flatten(Products.find().fetch().map(x => x.ingredients)));
    }
}
const name = 'ProductsService';

export default angular.module(name, [])
    .service(name, ProductsService);