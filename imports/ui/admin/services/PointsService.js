import angular from 'angular';
import {Points} from '../../../api/points';

Meteor.subscribe("points");

class PointsService {
    constructor($state, Notification) {
        'ngInject';

        this.$state = $state;
        this.Notification = Notification;
    }

    getPoints() {
        return Points.find();
    }

    add(name) {
        Meteor.call('points.add', name, err => {
            if (err) {
                this.Notification.error(err.message);
            } else {
                this.Notification.success(`Punkt ` + name + ` dodany!`);
                this.$state.go('admin.points');
            }
        });
    }

    remove(id, name) {
        Meteor.call('points.remove', id,  err => {
            if (err) {
                this.Notification.error(err.message);
            } else {
                swal(`Usunięto!`, `Punkt ` + name + ` został usunięty.`, "success");
                this.$state.go('admin.points');
            }
        });
    }

    update(point) {
        Meteor.call('points.update', point, err => {
            if (err) {
                this.Notification.error(err.message);
            } else {
                this.Notification.success(`Punkt ${point.name} zaktualizowany!`);
                this.$state.go('admin.points');
            }
        });
    }

    getPointById(_id) {
        return Points.findOne(_id);
    }
}
const name = 'PointsService';

export default angular.module(name, [])
    .service(name, PointsService);
