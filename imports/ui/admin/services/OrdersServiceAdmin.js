/**
 * Created by pawel on 28.05.16.
 */
import angular from 'angular';
import {Orders} from '../../../api/orders';
Meteor.subscribe('orders');

class OrdersServiceAdmin {
    constructor($state, $stateParams, Notification) {
        'ngInject';
        this.$state = $state;
        this.$stateParams = $stateParams;
        this.Notification = Notification;
    }

    getOrders(){
        return Orders.find();
    }
    getOrdersByStatus(statusOne){
        return Orders.find({
            status: statusOne
        });
    }
    
    getNewOrdersCount(){
        return Orders.find({status: "new"}).count();
    }

    remove(id, number) {
        Meteor.call('orders.remove', id,  err => {
            if (err) {
                this.Notification.error(err.message);
            } else {
                swal(`Usunięto!`, `Zamówienie nr ` + number +  ` zostało usunięte.`, "success");
                this.$state.go('admin.orders');
            }
        });
    }

    removeAll(){
        Meteor.call('orders.removeAll', err => {
            if (err){
                this.Notification.error(err.message);
            } else {
                swal("Usunięto!", "Wszystkie gotowe zamówienia zostały usunięte", "success");
                this.$state.go('admin.orders');
            }
        });
    }

    done(id, number) {
        Meteor.call('orders.done', id,  err => {
            if (err) {
                this.Notification.error(err.message);
            } else {
                swal(`Zamówienie nr ` + number +  ` jest gotowe.`,"", "success");
                this.$state.go('admin.orders');
            }
        });
    }

}
const name = 'OrdersServiceAdmin';

export default angular.module(name, [])
    .service(name, OrdersServiceAdmin);