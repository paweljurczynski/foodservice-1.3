import {Meteor} from 'meteor/meteor';
import angular from 'angular';
import {Messages} from '../../../api/messages';
import buzz from 'buzz';

Meteor.subscribe('messages');

class MessageService {
    constructor($state, $stateParams, Notification) {
        'ngInject';
        this.$state = $state;
        this.$stateParams = $stateParams;
        this.Notification = Notification;
    }

    getLastMessage() {
        return Messages.findOne({}, {sort: {created:-1, limit: 1}});
    }

    shouldDisplayMessage() {
        var sound = new buzz.sound('/public/sounds/demonstrative.mp3');
        sound.play();

        return Messages.find().count({isActive: true});
    }
}

const name = 'MessageService';

export default angular.module(name, [])
    .service(name, MessageService);
