/**
 * Created by pawel on 10.05.16.
 */
import { Mongo } from 'meteor/mongo';

const Orders = new Mongo.Collection("orders");


Orders.allow({
    insert: () => {
        return true;
    },
    update: () => {
        return true;
    },
    remove: () => {
        return true;
    }
});

export { Orders }
