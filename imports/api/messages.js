import {Mongo} from 'meteor/mongo';

const Messages = new Mongo.Collection("messages");

var messagesAmount = Messages.find().count();


Messages.allow({
    insert: () => {
        return true;
    },
    update: () => {
        return true;
    },
    remove: () => {
        return true;
    }
});

export {Messages, messagesAmount};

