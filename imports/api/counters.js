/**
 * Created by pawel on 21.07.16.
 */
import {Mongo} from 'meteor/mongo';

const Counters = new Mongo.Collection("counters");

const getNextSequence = function (name) {
    if (Counters.find({type: "orderId"}).count() === 0) {
        Counters.insert({type: "orderId", seq: 0});
    }

    Counters.update({type: name}, {$inc: {seq: 1}});
    const ret = Counters.findOne({type: name});
    return ret.seq;
};

export {Counters, getNextSequence}