import {Meteor} from 'meteor/meteor';
import {Categories} from '../../imports/api/categories';
Meteor.publish('categories', function (){
    return Categories.find();
});
Meteor.methods({
    'categories.add': (category) => {
        Categories.insert({
            name: category.name,
            priceTypes: category.priceTypes,
            companyId: "DsadSdasdDsR3A"
        });
    },
    'categories.update': (category, _id) => {
        Categories.update(
            { _id },
            {
                $set: {
                    name: category.name,
                    priceTypes: category.priceTypes
                }
            });
    },
    'categories.remove': (id) => {
        Categories.remove(id);
    }
});
