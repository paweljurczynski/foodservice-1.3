import {Meteor} from 'meteor/meteor';
import {Companies} from '../../imports/api/companies';

Meteor.publish('companies', function () {
    return Companies.find();
});