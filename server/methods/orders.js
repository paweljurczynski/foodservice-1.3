/**
 * Created by pawel on 21.07.16.
 */
import {Meteor} from 'meteor/meteor'
import {Orders} from '../../imports/api/orders'
import {getNextSequence} from '../../imports/api/counters';
Meteor.publish('orders', function (){
   return Orders.find();
});

function currDate()
{
    var clock = new Date();
    var Data ={
        month : clock.getMonth()+1,
        day : clock.getDay(),
        hour : clock.getHours(),
        minute : clock.getMinutes()
    }
    if (Data.month < 10)
        Data.month = "0" + Data.month;
    if (Data.day < 10)
        Data.day = "0" + Data.day;
    if (Data.hour < 10)
        Data.hour = "0" + Data.hour;
    if (Data.minute < 10)
        Data.minute = "0" + Data.minute;
    return Data;
}


Meteor.methods({
    'orders.add': (userData, products) => {
        const total = _.reduce(products, (acc, product) => {
            const productTotal = product.price * product.qty;
            return acc + productTotal;
        }, 0);
        
        Orders.insert({
            readableId: getNextSequence("orderId"),
            userData,
            products,
            total,
            status: "new",
            date: currDate()
        });


        this.order = Orders.findOne();
        Meteor.call('messages.orderAdd', order._id);
    },
    'orders.remove': (id) =>{
        Orders.remove(id);
    },

    'orders.removeAll': () => {
        Orders.remove({
            status: "done"
        });
    },

    'orders.done': (id) => {

        Orders.update(id,
            {
                $set: {
                    status: "done"
                }
            });
    }

});