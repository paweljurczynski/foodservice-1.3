/**
 * Created by Karol on 19.07.2016.
 */
import {Meteor} from 'meteor/meteor'
import {Products} from '../../imports/api/products'

Meteor.publish('products', function(){
   return Products.find();
});

Meteor.methods({
    'products.add': (product) => {
        Products.insert({
            companyId: "DsadSdasdDsR3A",
            name: product.name,
            categoryId: product.categoryId,
            prices: _.toArray(product.prices),
            ingredients: product.ingredients
        });
    },
    'products.update': (product, id) => {
        Products.update({
                _id: id
            }, {
                $set: {
                    name: product.name,
                    ingredients: product.ingredients,
                    categoryId: product.categoryId,
                    prices: product.prices
                }
            }
        );
    },
    'products.remove': (id) => {
        Products.remove(id);
    }
});