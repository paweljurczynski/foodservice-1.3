/**
 * Created by Karol on 05.08.2016.
 */
import {Meteor} from 'meteor/meteor'
import {Messages} from '../../imports/api/messages'

Meteor.publish('messages', function (){
    return Messages.find();
});

Meteor.methods({
    'messages.orderAdd': (orderId) => {

        Messages.insert({
            orderId,
            message: 'Otrzymano nowe zamówienie',
            dateCreated: Date.parse(new Date()),
    });
    }
});