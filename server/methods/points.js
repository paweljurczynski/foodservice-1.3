/**
 * Created by pawel on 22.07.16.
 */
import {Meteor} from 'meteor/meteor'
import {Points} from '../../imports/api/points'

Meteor.publish('points', () => {
    return Points.find();
});

Meteor.methods({
    'points.add': (point) => {
        point.companyId = "DsadSdasdDsR3A";
        //TODO: Sprawdz czy na pewno ten uzytkonik moze dodac ten punkt
        Points.insert(point);
    },
    'points.remove': (id) => {
        //TODO: Sprawdz czy na pewno ten uzytkonik moze usunąć ten punkt
        Points.remove(id);
    },
    'points.update': (point) => {
        //TODO: Sprawdz czy na pewno ten uzytkonik moze zaktualizować ten punkt
        Points.update(point.id,
            {
                $set: {
                    name: point.name,
                    location: {
                        type: 'point',
                        address: point.location.address,
                        coordinates: point.location.coordinates
                    }
                }
            });
    }
});