/**
 * Created by pawel on 22.07.16.
 */
if(Meteor.isServer) {
    if (Meteor.users.find().count() === 0) {
        const adminId = Accounts.createUser({
            username: 'Administrator',
            email: 'pawel.jurczynski@live.com',
            password: 'NHEgymG2!'
        });

        Roles.addUsersToRoles(adminId, ['admin']);

        const piecykowaId = Accounts.createUser({
            username: 'Piecykowa',
            email: 'piecykowa@hangri.pl',
            password: 'piecykowa'
        });

        Roles.addUsersToRoles(piecykowaId, ['owner']);
    }
}