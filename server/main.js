import '../imports/startup/fixtures';
import '../imports/api/products';
import '../imports/api/orders';
import '../imports/api/categories';
import '../imports/api/companies';
import '../imports/api/counters';

//Accounts.config({sendVerificationEmail: true, forbidClientAccountCreation: false});

Accounts.onCreateUser((options, user) => {
    Roles.addUsersToRoles(user._id, ['customer']);

    return user;
});